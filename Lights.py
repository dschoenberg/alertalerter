import RPi.GPIO as GPIO
import time
      
    


class Lights:
    #self.RED
    #self.GREEN
    #self.BLUE
    
    def __init__(self):
        #print('foo bar')
        self.setup()
        
    def setup(self):    
        #print('Setup')
        #used for GPIO numbering
        GPIO.setmode(GPIO.BOARD) 

        #closing the warnings when you are compiling the code
        GPIO.setwarnings(False)

        #defining the pins
        red = 36
        green = 38
        blue = 40
        red2 = 37
        green2 = 35

        #defining the pins as output
        GPIO.setup(red, GPIO.OUT) 
        GPIO.setup(green, GPIO.OUT)
        GPIO.setup(blue, GPIO.OUT)
        GPIO.setup(red2, GPIO.OUT) 
        GPIO.setup(green2, GPIO.OUT)

        #choosing a frequency for pwm
        Freq = 100

        #defining the pins that are going to be used with PWM
        self.RED = GPIO.PWM(red, Freq)  
        self.GREEN = GPIO.PWM(green, Freq)
        self.RED2 = GPIO.PWM(red2, Freq)  
        self.GREEN2 = GPIO.PWM(green2, Freq)
        self.BLUE = GPIO.PWM(blue, Freq)
        
        self.allOn()
        self.allOff()
    
    def showGood(self):
        self.allOff()
        self.GREEN.ChangeDutyCycle(100)
        self.GREEN2.ChangeDutyCycle(100)
        time.sleep(3)
        #self.pulseSlow(self.GREEN)
    
    def showBad(self):
        self.allOff()
        self.RED.ChangeDutyCycle(100)
        self.RED2.ChangeDutyCycle(100)
    
    def showMeh(self):
        self.allOff()
        self.RED.ChangeDutyCycle(100)
        self.RED2.ChangeDutyCycle(100)
        self.GREEN.ChangeDutyCycle(100)
        self.GREEN2.ChangeDutyCycle(100)
        
    def tearDown(self):
        self.RED.stop()
        self.RED2.stop()
        self.GREEN.stop()
        self.GREEN2.stop()
        self.BLUE.stop()
        GPIO.cleanup()    
            
    def allOn(self):
        #print('All On')
        self.RED.start(100)
        self.RED2.start(100)
        self.GREEN.start(100)
        self.GREEN2.start(100)
        self.BLUE.start(100)
        time.sleep(3)
            
    def allOff(self):
        #print('All Off')
        self.RED.ChangeDutyCycle(0)
        self.RED2.ChangeDutyCycle(0)
        self.GREEN.ChangeDutyCycle(0)
        self.GREEN2.ChangeDutyCycle(0)
        self.BLUE.ChangeDutyCycle(0)
    
    def pulseSlow(self, Bulb):
        self.Pulse=True
        while self.Pulse:
            for x in range(1,101):
                Bulb.ChangeDutyCycle(101-x)
                time.sleep(0.025)
            for x in range(1,101):
                Bulb.ChangeDutyCycle(x)
                time.sleep(0.025)
            time.sleep(3)
        

    def turnMeOn(self):
        #defining the RPi's pins as Input / Output
        
        RUNNING = True
        try:
            #we are starting with the loop
            while RUNNING:
                print('Show Good')
                self.showGood()
                time.sleep(3)
                print('Show Bad')
                self.showBad()
                time.sleep(3)
                print('Show Meh')
                self.showMeh()
                time.sleep(3)
                
        except KeyboardInterrupt:
            # the purpose of this part is, when you interrupt the code, it will stop the while loop and turn off the pins, which means your LED won't light anymore   
            print('Quitter')
            RUNNING = False
            self.allOff()
            GPIO.cleanup()

