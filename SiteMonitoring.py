import requests

class SiteMonitoring:
   def monitor(self):
       try:
            rLoad = requests.get('https://hackbeardsmiths.azurewebsites.net/kickload.aspx')
            if rLoad.status_code == 404:
                r1 = requests.get('https://hackbeardsmiths.azurewebsites.net/')    
                if r1.status_code == 200:
                    return 1
                elif r1.status_code == 500:
                    return 3
                elif r1.status_code == 403:
                    return 3
            elif rLoad.status_code == 200:
                return 2
       except:
            return 3
