import time
import pygame

#our stuff
from Lights import *
from SiteMonitoring import *
from ScreenManager import *
from SoundManager import *

pygame.init()

sleepyTime = 2

lights = Lights()
alert = SiteMonitoring()
sm = ScreenManager()
sound = SoundManager()

#sound.soundup()

sm.setup(True)
sm.showLoadingScreen()
time.sleep(1)
sm.showLoadingScreen2()

if __name__ == "__main__":
    try:
        while True:
            
            status = alert.monitor()
            if status == 1:
                sm.showHappyScreen()
                lights.showGood()
            elif status == 2:
                sm.showMehScreen()
                lights.showMeh()
            elif status == 3:
                sm.showBadScreen()
                sound.soundup()
                lights.showBad()
            else:
                sm.showBadScreen()
                lights.showBad()
                sound.soundup()
                print("unknown status")
            time.sleep(sleepyTime)
            events = pygame.event.get()
            for event in events:
                if event.type == pygame.KEYDOWN:
                    lights.tearDown()
                    quit()
                    
    except KeyboardInterrupt:
        lights.tearDown()
        quit()
        
    
    
    #showImage()
    