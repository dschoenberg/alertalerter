import pygame
from pygame.locals import *

WIDTH = 640
HEIGHT = 480

class ScreenManager:
    def __init__(self): 
        pass
    
    def setup(self, fullScreen):
        if(fullScreen):
            print("Starting fullscreen")
            self.windowSurface = pygame.display.set_mode((WIDTH, HEIGHT), pygame.FULLSCREEN)
        else:
            print("Starting windowed")
            self.windowSurface = pygame.display.set_mode((WIDTH, HEIGHT))
            
    def showLoadingScreen(self):
        self.showImage('startup.png')
        
    def showLoadingScreen2(self):
        self.showImage('startup2.png')
        
    def showHappyScreen(self):
        self.showImage('good.png')
        
    def showBadScreen(self):
        self.showImage('bad.png')
        
    def showMehScreen(self):
        self.showImage('meh.png')
        
    def showImage(self, fileName):
        print('showing image' + fileName)
        img = pygame.image.load("images/" + fileName)
        self.windowSurface.blit(img, (0,0))
        pygame.display.update()
